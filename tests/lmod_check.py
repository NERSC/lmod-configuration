import os
assert os.getenv("LMOD_SITE_NAME") == "NERSC"
assert os.getenv("LMOD_SYSTEM_NAME") == "perlmutter"
assert os.getenv("LMOD_SYSHOST") == "perlmutter"
assert os.getenv("LMOD_PACKAGE_PATH")
assert os.getenv("LMOD_RC") == os.path.join(os.getenv("LMOD_PACKAGE_PATH"),"lmodrc.lua")
assert os.getenv("LMOD_CASE_INDEPENDENT_SORTING") == "yes"
assert os.getenv("LMOD_AVAIL_STYLE") == "grouped:system"
