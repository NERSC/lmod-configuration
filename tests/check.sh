#!/bin/bash  
set -e
python3 lmod_check.py

# check if we can load all the PrgEnv compilers one after the other. It should autoswap modules
names="PrgEnv-cray PrgEnv-aocc PrgEnv-gnu PrgEnv-nvidia"
for name in $names
do
  module load $name
  if [[ $LMOD_FAMILY_PRGENV != $name ]]; then
    echo "environment variable LMOD_FAMILY_PRGENV is not $name"
    exit 1
  fi
done

