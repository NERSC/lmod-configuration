local i18n = require("i18n")
propT = {
   lmod = {
      validT = { sticky = 1 },
      displayT = {
         sticky = { short = "(S)",  long = "(S)",   color = "red", doc = "Module is Sticky, requires --force to unload or purge",  },
      },
   },
   type_ = {
      validT = { compiler= 1, mpi = 2, craype=3, dev = 4, math = 5, vis = 6, io = 7, ai = 8, debuggers = 9, buildtools = 10 },
      displayT = {
         ["compiler"]  = { short = "(c)",  long = "(compiler)",  full_color = true,  color = "blue", doc = "Compiler", },
         ["mpi"]       = { short = "(mpi)",  long = "(mpi)",   full_color = true, color = "red", doc = "MPI Providers", },
         ["craype"]       = { short = "(cpe)",  long = "(cpe)",  full_color = true, color = "green", doc = "Cray Programming Environment Modules",  },
         ["dev"]       = { short = "(dev)",  long = "(dev)",   full_color = true, color = "magenta", doc = "Development Tools and Programming Languages", },
         ["math"]      = { short = "(math)",  long = "(math)", full_color = true,  color = "green", doc = "Mathematical libraries", },
         ["vis"]       = { short = "(vis)",  long = "(vis)",   full_color = true, color = "blue", doc = "Visualization software" , },
         ["io"]        = { short = "(io)",  long = "(io)",   full_color = true, color = "yellow", doc = "Input/output software"  },
         ["ai"]        = { short = "(ai)",  long = "(ai)",   full_color = true, color = "cyan", doc = "Artificial Intelligence", }, 
         ["debuggers"] = { short = "(debuggers)",  long = "(debuggers)", full_color = true,  color = "cyan", doc = "Debuggers", }, 
         ["buildtools"] = { short = "(buildtools)",  long = "(buildtools)", full_color = true,  color = "blue", doc = "Software Build Tools", },
     },
   },

  status = {
    validT = { active = 1, },
    displayT = {
       active = { short = "(L)",  long = "(Loaded)", full_color = true,    color = "green", doc = i18n("LoadedM")},
    }  ,
  }
}
-- need to decide location where spider generates spider cache see https://lmod.readthedocs.io/en/latest/130_spider_cache.html#how-to-test-the-spider-cache-generation-and-usage
scDescriptT = {
  {
    ["dir"]       = "$LMOD_SPIDER_CACHE_DIR/cacheDir",
    ["timestamp"] = "$LMOD_SPIDER_CACHE_DIR/system.txt",
  },
}
