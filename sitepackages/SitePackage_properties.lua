function set_props(t)
   ------------------------------------------------------------
   -- table of properties for fullnames or sn

   local propT = {
      [ { "aocc", "gcc", "nvidia", "craype", "cuda", "llvm", "nvhpc", "GCC" } ]       = { {name = "type_", value = "compiler" }, },
      [ { "PrgEnv-cray", "PrgEnv-gnu", "PrgEnv-aocc", "PrgEnv-nvidia", "cpe", "cpe-cuda", "craype-x86-milan", "craype-x86-milan-x", "craype-x86-rome", "craype-x86-trento" } ]  = { {name = "type_", value = "craype" }, },
      [ { "cray-mpich", "cray-mpich-abi", "mpich", "openmpi", "OpenMPI" } ]  = { {name = "type_", value = "mpi" }, },
      [ { "cray-python", "cray-R", "papi", "perftools", "perftools-base", "perftools-lite", "perftools-lite-events", "perftools-lite-gpu", "perftools-lite-hbm", "perftools-lite-loops", "perftools-preload", "python", "cray-R", "Python"} ] = { {name = "type_", value = "dev" }, },
      [ { "cray-fftw", "cray-libsci", "openblas", "netlib-lapack", "netlib-scalapack", "fftw", "FFTW", "OpenBLAS", "ScaLAPACK" } ] = { {name = "type_", value = "math" }, },
      [ { "cray-hdf5", "cray-hdf5-parallel", "cray-netcdf", "cray-netcdf-hdf5parallel", "cray-parallel-netcdf", "darshan" } ]  = { { name = "type_", value = "io" } },
      [ { "gdb4hpc", "valgrind4hpc", "cray-stat", "atp" } ] = { { name = "type_", value = "debuggers" } },
      [ { "cuda", "nvidia", "cudatoolkit", "CUDA" } ] = { { name = "arch", value = "gpu" } },
      [ { "cmake", "e4s", "nersc-easybuild", "spack", "Autoconf", "Automake", "CMake", "EasyBuild" } ] = { { name = "type_", value = "buildtools" } },
   }

   for k,v in pairs(propT) do
     ------------------------------------------------------------
     -- Look for fullName first otherwise sn
     if (has_value(k,myModuleFullName()) or has_value(k,myModuleName())) then
        ----------------------------------------------------------
        -- Loop over value array and fill properties for this module.
        for i = 1,#v do
           local entry = v[i]
           add_property(entry.name, entry.value)
	   whatis("Keyword:" .. entry.value)
        end
     end
   end
end
