
local hook = require("Hook")
function dofile (filename)
	local f = assert(loadfile(filename))
	return f()
end
function has_value(tab, val)
	for index, value in ipairs(tab) do
		if value == val then
			return true
		end
	end
	return false
end
local lmod_package_path = os.getenv("LMOD_PACKAGE_PATH")
dofile(pathJoin(lmod_package_path,"SitePackage_properties.lua"))
-- Do we want to configure Lmod Families for Perlmutter, if no remove line below
dofile(pathJoin(lmod_package_path,"SitePackage_families.lua"))

local function load_hook(t)
	set_props(t)

-- Do we want to configure Lmod Families for Perlmutter, if no remove line below
	set_family(t)
end

local function unload_hook(t)
-- Do we want to configure Lmod Families for Perlmutter, if no remove line below
	set_family(t)
end

hook.register("load",load_hook)

local mapT = 
{ 
  grouped = {
    [ '/opt/cray/pe/lmod/modulefiles/mpi/*' ] = "Cray MPI Dependent Modules",
    [ '/opt/cray/pe/lmod/modulefiles/compiler/*' ] = "Cray Compiler Dependent Modules",
    [ '/opt/cray/pe/lmod/modulefiles/core/*'] = "Cray Core Modules",
    [ '/opt/cray/pe/lmod/modulefiles/net/*'] = "Cray Network Dependent Packages",
    [ '/opt/cray/pe/lmod/modulefiles/comnet/*'] = "Cray Compiler/Network Dependent Packages",
    [ '/opt/cray/pe/lmod/modulefiles/perftools/*'] = "Cray Performance Tools",
    [ '/opt/cray/modulefiles' ] = "Cray Modules",
    [ '/opt/modulefiles/*'] = "Cray Compilers",
    [ '/usr/share/lmod/lmod/modulefiles/Core/*'] = "Lmod Modulefiles",
    [ '/global/common/software/nersc/shasta2105/modulefiles' ] = "NERSC-provided Software",
    [ '/global/common/software/nersc/shasta2105/extra_modulefiles' ] = "Additional available software",
  },
}

function avail_hook(t)
   local availStyle = masterTbl().availStyle
   local styleT     = mapT[availStyle]
   if (not availStyle or availStyle == "system" or styleT == nil) then
      return
   end

   for k,v in pairs(t) do
      for pat,label in pairs(styleT) do
         if (k:find(pat)) then
            t[k] = label
            break
         end
      end
   end
end


hook.register("avail",avail_hook)

