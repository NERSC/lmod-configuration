-- default versions
module_version("cmake/3.22.0","default")
-- hide versions
hide_version("cray-lustre-client/2.12.4.2_cray_62_gd308b9d-7.0.1.0_6.3__gd308b9d2d1.shasta")
hide_version("dvs/2.12_4.0.110-7.0.1.0_13.1__g4e091855")
hide_version("xpmem/2.2.40-7.0.1.0_2.4__g1d7a24d.shasta")
