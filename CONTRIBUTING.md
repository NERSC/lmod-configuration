# CONTRIBUTING GUIDE

This guide outlines the contribution process for making changes to Lmod configuration for Perlmutter. In order to get started login to perlmutter and clone this repo in your user space ($HOME).

Currently, permutter is only accessible from cori so once you login you can ssh to perlmutter as follows:

## Setup

```
ssh perlmutter
```

Next clone this repo and navigate to the directory 

```
git clone https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration.git
cd lmod-configuration
```

## Contribution Process

The contribution process is relatively simple, first create a feature branch off `master` then add your local changes via `git add` followed by committing your changes and push those changes upstream followed by PR

```
git checkout master
git checkout -b featureX
git add <file1> <file2>
git commit -m <commit-message>
git push origin featureX
```

Now that `featureX` branch is pushed upstream you can issue a Merge Request by clicking [here](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/merge_requests/new) and select the **Source branch** to 
your branch (i.e `featureX`) and **Target branch** to [master](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/tree/master).

## Lmod configuration files

- [zzz-lmod.csh.j2](https://gitlab.nersc.gov/nersc/csg/nersc-cle/-/blob/development/shasta/ansible/nersc/roles/nersc-user/templates/zzz-lmod.csh.j2) and [zzz-lmod.sh.j2](https://gitlab.nersc.gov/nersc/csg/nersc-cle/-/blob/development/shasta/ansible/nersc/roles/nersc-user/templates/zzz-nerscenv.sh.j2) - are Lmod configuration repo in Jinja2 template which is deployed via ansible. These files are pushed to `/etc/profile.d/zzz-lmod.[sh|csh]` for Perlmutter and Muller.
- [z01_lmod.sh](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/z01_lmod.sh) - this is the Lmod configuration for *bash* and *sh* shell which is sourced at startup. 
- [sitepackages](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/tree/master/sitepackages) - the sitepackages directory is used with `SitePackage.lua` script for defining hooks. The environment variable **LMOD_PACKAGE_PATH** sets path to this directory where Lmod will find our [SitePackage.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/SitePackage.lua) file with out site customization. These files should not be renamed as it will impact production. For more details SitePackage.lua and hooks see https://lmod.readthedocs.io/en/latest/170_hooks.html 
- [SitePackage.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/SitePackage.lua) - entry point to SitePackages.lua file that defines hooks for Lmod commands. 
- [SitePackages_families.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/SitePackage_families.lua) - lua function to define family key/value pair for Lmod families. This file should be updated if you want to add modules to a particular family group
- [SitePackages_properties.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/SitePackage_properties.lua) - this lua function defines [module properties](https://lmod.readthedocs.io/en/latest/145_properties.html). This file can be updated to add a new module name to an existing property. If you want to add a new property please define a new property in [lmodrc.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/lmodrc.lua) and then reference in this file
- [modulerc.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/modulerc.lua) - this file is used for controlling defaults, or hiding version. Lmod will use this file because we have set `LMOD_MODULERCFILE` variable to this file. For more details see https://lmod.readthedocs.io/en/latest/093_modulerc.html. This file should be updated if one wants to hide a particular module or change defaults for a module file
- [lmodrc.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/lmodrc.lua) - This is the Lmod properties file which is used for defining module property states. Lmod provides a default Lmod property located at `$LMOD_PKG/init/lmodrc.lua` which comes with Lmod installation however we define our own. The environment variable **LMOD_RC** points to this file so that Lmod can read this file to register new properties.

## Production Deployment

This repo is cloned at `/global/common/software/nersc/[perlmutter|muller]` with one Lmod configuration for **Perlmutter** and **Muller**. The files are owned by user **swowner** which is a shared collaboration account accessible by all NERSC consultants. Please make sure that changes to upstream repo get synced locally. 

Please make sure that all subdirectories have read/execute permission (`o+rx`) for subdirectories including files under `sitepackages` folder as shown below


```
swowner@cori12:/global/common/software/nersc> ls -l perlmutter/lmod-configuration/sitepackages/
total 13
-rwxrwxr-x 1 swowner nstaff 2122 Aug 30 07:24 SitePackage.lua
-rwxrwxr-x 1 swowner nstaff  647 Aug 30 07:24 SitePackage_families.lua
-rwxrwxr-x 1 swowner nstaff 1719 Aug 30 07:24 SitePackage_properties.lua
-rwxrwxr-x 1 swowner nstaff 2133 Aug 30 07:24 lmodrc.lua
-rwxrwxr-x 1 swowner nstaff  288 Aug 30 07:24 modulerc.lua
```

**Please do not make any changes locally via `swowner` account** 

