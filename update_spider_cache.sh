#!/bin/bash
# This script will update the System Spider Cache which should run on a scheduled basis. Ideally this should be managed by CI job for more details see https://lmod.readthedocs.io/en/latest/130_spider_cache.html#system-spider-cache 
module load lmod

update_lmod_system_cache_files -d $LMOD_SPIDER_CACHE_DIR/cacheDir -t $LMOD_SPIDER_CACHE_DIR/system.txt $MODULEPATH
