# Lmod Configuration

This repository contains Lmod configuration for Perlmutter and Muller system.

The startup Lmod configuration scripts (`/etc/profile.d/zzz-lmod.[sh|csh]`) are defined in project [nersc-cle](https://gitlab.nersc.gov/nersc/csg/nersc-cle) at https://gitlab.nersc.gov/nersc/csg/nersc-cle/-/tree/development/shasta/ansible/nersc/roles/nersc-user with Jinja2 templates. If you want to update these files you can copy them locally and source the files to update the Lmod configuration locally and check the configuration by running

```
module --config
```


# Noteable Changes

- We have changed default output format of `module avail` by setting `LMOD_AVAIL_STYLE="grouped:system"` which will show text as pose to full path of moduletree. This can be used to distinguish 
module trees by meaingful names. You can revert back to original output style using `module -s system avail`.

- Define `LMOD_PACKAGE_PATH` with location to manage Lmod hooks which are defined in [sitepackages](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/tree/master/sitepackages) folder

- Define [lmodrc.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/lmodrc.lua) file that configures Lmod setting which is set via `export LMOD_RC=$LMOD_PACKAGE_PATH/lmodrc.lua`. This allows us to manage Lmod configuration without relying on system Lmod configuration that is owned by `root`.

- If you load one of the modules defined in [SitePackages_properties.lua](https://gitlab.nersc.gov/nersc/consulting/software-configuration/lmod-configuration/-/blob/master/sitepackages/SitePackage_properties.lua) and run `module list` you should see some with tags. Shown below is an example with `(m)` and `(t)` property tags for modules. 

```
$ module list

Currently Loaded Modules:
  1) cce/11.0.2            4) libfabric/1.11.0.0.233   7) perftools-base/21.02.0                    (t)  10) cray-libsci/20.12.1.2
  2) craype/2.7.5    (c)   5) craype-network-ofi       8) xpmem/2.2.35-7.0.1.0_1.9__gd50fabf.shasta      11) PrgEnv-cray
  3) craype-x86-rome       6) cray-dsmml/0.1.3         9) cray-mpich/8.1.2                          (m)

  Where:
   m:  MPI Providers
   c:  Compiler
   t:  Tools for development

```

# Running Tests

Login to muller and make sure you have sourced the `z01_lmod.sh` script. To run the tests you should be in `tests` directory and then run the shell script as follows


```
cd tests
sh check.sh
```

# References

- https://lmod.readthedocs.io/en/latest/index.html
- Most of the Lmod configuration with Lmod hooks are based on Compute Canda project https://github.com/ComputeCanada/software-stack-config
